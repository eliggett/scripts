#!/bin/bash

if [ -n "$BASH_VERSION" ]; then
    echo "Scanning for radios..."
else
    echo "Script was likely run by sh (sh script.sh or similar)"
    echo "Please re-run using ./$0 as the method."
    exit 1
fi

# Check if script is being redirected
if [ -t 1 ]; then
    LOG_FILE="/tmp/$(basename "$0")-$(date '+%Y%m%d-%H%M%S').log"
    # Tee output to both console and log file
    exec 1> >(tee "$LOG_FILE")
    exec 2>&1
fi

echo "Script started at $(date)"


# Print header
echo "--- USB Serial devices ---"
shopt -s nullglob
for device in /dev/ttyUSB*; do
	ls -l $device
done

printf "%-15s %-20s %-20s %-20s\n" "Device" "Serial" "Vendor" "Model"
printf "%-15s %-20s %-20s %-20s\n" "$(printf '%.15s' "---------------")" "$(printf '%.20s' "--------------------")" "$(printf '%.20s' "--------------------")" "$(printf '%.20s' "--------------------")"

# Find and process each USB device
for device in /dev/ttyUSB*; do
    # Skip if no devices found
    [[ -e "$device" ]] || { echo "No USB serial devices found"; }
    
    # Get device information using udevadm
    serial=$(udevadm info "$device" | grep ID_SERIAL_SHORT | cut -d= -f2)
    vendor=$(udevadm info "$device" | grep ID_VENDOR_FROM_DATABASE | head -n1 | cut -d= -f2)
    model=$(udevadm info "$device" | grep ID_MODEL= | head -n1 | cut -d= -f2)
    
    # Print formatted row
    printf "%-15s %-20s %-20s %-20s\n" \
        "$(basename "$device")" \
        "${serial:-N/A}" \
        "${vendor:-N/A}" \
        "${model:-N/A}"
done

printf "\n"
# Print header
echo "--- Sound devices ---"
printf "%-15s %-30s %-30s %-20s\n" "Card" "Vendor" "Model" "Serial"
printf "%-15s %-30s %-30s %-20s\n" "$(printf '%.15s' "---------------")" "$(printf '%.30s' "------------------------------")" "$(printf '%.30s' "------------------------------")" "$(printf '%.20s' "--------------------")"

# Find and process each sound card
for card_path in /sys/class/sound/card*; do
    # Skip if no cards found
    [[ -e "$card_path" ]] || { echo "No sound cards found";  }
    
    card_name=$(basename "$card_path")
    
    # Get the device path for udevadm
    device_path=$(readlink -f "$card_path")
    
    # Get device information using udevadm
    udevinfo=$(udevadm info --path="$device_path" --query=property)
    
    # Extract information using various possible property names
    vendor=$(echo "$udevinfo" | grep -E "ID_VENDOR=|ID_VENDOR_FROM_DATABASE=" | head -n1 | cut -d= -f2)
    # If vendor not found, try alternate location
    if [ -z "$vendor" ]; then
        vendor=$(cat "$device_path/id/vendor" 2>/dev/null || echo "N/A")
    fi
    
    model=$(echo "$udevinfo" | grep -E "ID_MODEL=|ID_MODEL_FROM_DATABASE=" | head -n1 | cut -d= -f2)
    # If model not found, try alternate location
    if [ -z "$model" ]; then
        model=$(cat "$device_path/id/model" 2>/dev/null || echo "N/A")
    fi
    
    serial=$(echo "$udevinfo" | grep -E "ID_SERIAL=|ID_SERIAL_SHORT=" | head -n1 | cut -d= -f2)
    # If serial not found, try alternate location
    if [ -z "$serial" ]; then
        serial=$(cat "$device_path/id/serial" 2>/dev/null || echo "N/A")
    fi
    
    # Print formatted row
    printf "%-15s %-30s %-30s %-20s\n" \
        "$card_name" \
        "${vendor:-N/A}" \
        "${model:-N/A}" \
        "${serial:-N/A}"
done
echo ""
echo "--- ALSA-recognized sound devices from aplay: ---"
aplay -l
echo ""
echo "---------- Scan finished. ----------"
echo "Results were logged to $LOG_FILE."
echo ""
echo "Execute the following line to send to termbin and receive a URL,"
echo "which you can share on our support forum:"
echo ""
echo "cat $LOG_FILE | nc termbin.com 9999"
shopt -u nullglob


