#!/bin/bash

# Check if refresh rate argument is provided
if [ $# -ne 1 ]; then
    echo "Usage: $0 <refresh_rate_hz>"
    exit 1
fi

# Validate refresh rate is a positive number
refresh_rate=$1
if ! [[ $refresh_rate =~ ^[0-9]+([.][0-9]+)?$ ]] || [ $(echo "$refresh_rate <= 0" | bc -l) -eq 1 ]; then
    echo "Error: Refresh rate must be a positive number"
    exit 1
fi

# Calculate sleep duration in seconds
sleep_duration=$(echo "scale=3; 1/$refresh_rate" | bc)

# Initialize array for last 10 readings
declare -a last_readings=()
for i in {1..10}; do
    last_readings+=("0")
done
current_index=0

# Function to get total CPU usage across all cores
get_cpu_usage() {
    # Get CPU stats
    local cpu_stats=$(grep '^cpu ' /proc/stat)
    local user=$(echo $cpu_stats | awk '{print $2}')
    local nice=$(echo $cpu_stats | awk '{print $3}')
    local system=$(echo $cpu_stats | awk '{print $4}')
    local idle=$(echo $cpu_stats | awk '{print $5}')
    local iowait=$(echo $cpu_stats | awk '{print $6}')
    local irq=$(echo $cpu_stats | awk '{print $7}')
    local softirq=$(echo $cpu_stats | awk '{print $8}')
    local steal=$(echo $cpu_stats | awk '{print $9}')

    # Calculate total and idle time
    local total_time=$((user + nice + system + idle + iowait + irq + softirq + steal))
    local idle_time=$((idle + iowait))

    echo "$total_time $idle_time"
}

# Function to draw the bar meter
draw_bar() {
    local percentage=$1
    local peak_percentage=$2
    local term_width=$(tput cols)
    local prefix="CPU: "
    local suffix=" ${percentage}% (Peak: ${peak_percentage}%)"
    local bar_max_width=$((term_width - ${#prefix} - ${#suffix}))
    local bar_width=$((percentage * bar_max_width / 100))
    local peak_position=$((peak_percentage * bar_max_width / 100))
    
    # Ensure bar_width is at least 0
    if [ $bar_width -lt 0 ]; then
        bar_width=0
    fi
    
    # Ensure peak_position is within bounds
    if [ $peak_position -gt $bar_max_width ]; then
        peak_position=$bar_max_width
    fi

    # Draw the bar
    echo -n "$prefix"
    
    # Draw each character of the bar
    for ((i=0; i<bar_max_width; i++)); do
        if [ $i -eq $peak_position ]; then
            echo -n "|"
        elif [ $i -lt $bar_width ]; then
            echo -n "="
        else
            echo -n " "
        fi
    done
    
    echo -n "$suffix"
}

# Function to get peak value from array
get_peak() {
    local peak=0
    for value in "${last_readings[@]}"; do
        if [ "$value" -gt "$peak" ]; then
            peak=$value
        fi
    done
    echo $peak
}

# Get initial CPU stats
read total_prev idle_prev <<< $(get_cpu_usage)

# Main loop
while true; do
    # Sleep for the specified duration
    sleep $sleep_duration
    
    # Get current CPU stats
    read total_curr idle_curr <<< $(get_cpu_usage)
    
    # Calculate CPU usage percentage
    total_diff=$((total_curr - total_prev))
    idle_diff=$((idle_curr - idle_prev))
    cpu_usage=$(( 100 * (total_diff - idle_diff) / total_diff ))
    
    # Update circular buffer of readings
    last_readings[$current_index]=$cpu_usage
    current_index=$(( (current_index + 1) % 10 ))
    
    # Get peak value
    peak_usage=$(get_peak)
    
    # Clear line and draw bar
    echo -en "\r"
    draw_bar $cpu_usage $peak_usage
    
    # Update previous values
    total_prev=$total_curr
    idle_prev=$idle_curr
done
