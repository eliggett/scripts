#!/bin/bash

# Function to get interface speed
get_interface_speed() {
    if [ -f "/sys/class/net/$1/speed" ]; then
        speed=$(cat "/sys/class/net/$1/speed" 2>/dev/null)
        if [ $? -eq 0 ] && [ -n "$speed" ]; then
            echo "${speed} Mbps"
        else
            echo "NA"
        fi
    else
        echo "NA"
    fi
}

# Function to get IP addresses
get_ip_addresses() {
    local interface="$1"
    local ipv4=$(ip -4 addr show "$interface" | grep -oP '(?<=inet\s)\d+(\.\d+){3}' | tr '\n' ' ')
    local ipv6=$(ip -6 addr show "$interface" | grep -oP '(?<=inet6\s)[\da-f:]+' | tr '\n' ' ')
    
    if [ -z "$ipv4" ] && [ -z "$ipv6" ]; then
        echo "No IP assigned"
    else
        echo "IPv4: ${ipv4:-None} | IPv6: ${ipv6:-None}"
    fi
}

# Get list of all network interfaces
interfaces=$(ip -a link show | grep -oP '^\d+:\s*\K[^:@]+')

# Print header
echo "----- List of Network Interfaces -----"
printf "%-15s %-10s %-20s %s\n" "Interface" "Status" "Speed" "IP Addresses"
printf "%-15s %-10s %-20s %s\n" "---------" "------" "-----" "-------------"

# Iterate through interfaces
for interface in $interfaces; do
    # Check interface status
    status=$(ip link show "$interface" | grep -oP '(?<=state\s)[A-Z]+')
    
    # Get interface speed
    speed=$(get_interface_speed "$interface")
    
    # Get IP addresses
    ips=$(get_ip_addresses "$interface")
    
    # Print interface information
    printf "%-15s %-10s %-20s %s\n" "$interface" "$status" "$speed" "$ips"
done

printf "\n----- Default route: -----\n"
route -n | grep UG

printf "\n----- Public IP address (if available, press control-c to cancel) -----\n"
printf "Method 1: \n"
curl -m 120 -w '\n' -sfL 'api{4,6}.ipify.org'
printf "Method 2: \n"
wget -q -O - ipinfo.io/ip
printf "\nMethod 3: \n"
curl -s https://jsonip.com | sed 's/{"ip":"//; s/"}//'
printf "\nDone\n"

