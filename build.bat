ECHO Starting Build Script

call "C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\VC\Auxiliary\Build\vcvars32.bat"

cd \Users\eliggett\Documents\projects\wfview-master\wfview

git fetch

git pull

cd ..

mkdir build

cd build
nmake clean
qmake ..\wfview\wfview.pro

nmake

cd release

scp wfview.exe winbuild@linuxserver:

ECHO Finished building. 

